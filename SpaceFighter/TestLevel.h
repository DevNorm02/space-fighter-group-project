#pragma once

#include "Level.h"

/*TEST LEVEL HEADER FILE, USE IN TANDAM WITH THE MAIN 'TESTLEVEL.CPP' FILE*/

class TestLevel : public Level
{

public:

	TestLevel() { }

	virtual ~TestLevel() { }

	virtual void LoadContent(ResourceManager* pResourceManager);

	virtual void UnloadContent() { }
};

