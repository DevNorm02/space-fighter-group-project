//Level Includes
#include "TestLevel.h"

//Enemy Inclides
#include "BioEnemyShip.h"

//Powerup Includes





/*THIS LEVEL WILL NOT BE USED IN THE GAME, INSTEAD COPY IT FROM THE BRANCH, AND EDIT TO YOUR LIKING.
* THIS LEVEL IS MEANT TO BE USED BY EACH DEVELOPER TO EDIT AND TEST ALL ASSETS.
*/

void TestLevel::LoadContent(ResourceManager* pResourceManager)
{
	// Setup enemy ships
	Texture* pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	const int COUNT = 6; //Determines number of enemies in the level.


	//Positions of each enemy spawn on x-axis (range is 0 -> 1)
	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
	};

	//Spawn delay for each enemy. Each enemy spawn refers to the previous enemy spawn.
	double delays[COUNT] =
	{
		0.0, 0.1, 0.1,
		0.1, 0.1, 0.1,
	};


	float delay = 0.25; // start delay -- delay from when enemies start to spawn into the level
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip* pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);
}