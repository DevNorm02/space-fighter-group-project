#include "GameplayScreen.h"
#include "Level.h"
#include "Level01.h"
#include "TestLevel.h"


GameplayScreen::GameplayScreen(const int levelIndex)
{	
	m_pLevel = nullptr;
	switch (levelIndex)
	{
	case 0: m_pLevel = new Level01(); break;
	//TO ACCESS TEST LEVEL -- UNCOMMENT BELOW LINE AND COMMENT OUT ABOVE LINE. RESET TO GO BACK TO REGULAR LEVEL ORDER.
	//case 0: m_pLevel = new TestLevel(); break;

	}
	

	SetTransitionInTime(.5f);
	SetTransitionOutTime(0.2f);

	Show();
}

void GameplayScreen::LoadContent(ResourceManager *pResourceManager)
{
	m_pLevel->LoadContent(pResourceManager);
}

void GameplayScreen::HandleInput(const InputState *pInput)
{
	m_pLevel->HandleInput(pInput);
}

void GameplayScreen::Update(const GameTime *pGameTime)
{
	m_pLevel->Update(pGameTime);
	
}

void GameplayScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();

	m_pLevel->Draw(pSpriteBatch);

	pSpriteBatch->End();
}

void GameplayScreen::SetScreenManager(ScreenManager *pScreenManager)
{
	m_pLevel->SetScreenManager(pScreenManager);
}




